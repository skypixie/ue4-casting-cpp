// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BPCommunicationGameMode.generated.h"

UCLASS(minimalapi)
class ABPCommunicationGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABPCommunicationGameMode();
};



