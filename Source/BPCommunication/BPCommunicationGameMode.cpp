// Copyright Epic Games, Inc. All Rights Reserved.

#include "BPCommunicationGameMode.h"
#include "BPCommunicationCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABPCommunicationGameMode::ABPCommunicationGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
